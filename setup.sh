#!/usr/bin/env bash

if [[ $EUID == 0 ]]; then
   echo "This script must be run as non-root user inside docker group"
   echo "Run: sudo groupadd docker"
   echo "Run: sudo gpasswd -a \$USER docker"
   echo "Run newgrp docker or log out/in to activate changes"
   exit 1
fi

dir_name=$(cat setup.cfg | grep dir_name | sed 's/dir_name = //')
home_mount_path=$(cat setup.cfg | grep home_mount_path | sed 's/home_mount_path = //')
repo_url=$(cat setup.cfg | grep repo_url | sed 's/repo_url = //')
mount_path=$(echo "$HOME/$home_mount_path")

git clone $repo_url "$mount_path/$dir_name"
sed "s/<mount_path>/${mount_path//\//\\/}/" docker-compose.yml.j2 > docker-compose.yml
docker-compose -f docker-compose.yml up -d --build --force-recreate
